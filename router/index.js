const http = require ("http");
const express = require ("express"); 
//objeto ruta dentro de express
const router = express.Router();
const bodyParser = require("body-parser");

//obtener
router.get("/contrato", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		nivel: req.body.nivel,
		dias: req.body.dias,
		pagoDiario: req.body.pagoDiario,
		pSubtotal: req.body.pSubtotal,
		pImpuestos: req.body.pImpuestos,
		pTotal: req.body.pTotal,
		
	};
	res.render("contrato.html", params);
});

//publicar
router.post("/contrato", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		nivel: req.body.nivel,
		dias: req.body.dias,
		pagoDiario: req.body.pagoDiario,
		pSubtotal: req.body.pSubtotal,
		pImpuestos: req.body.pImpuestos,
		pTotal: req.body.pTotal,
	};
	res.render("contrato.html", params);
});


//obtener
router.get("/registros", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		nivel: req.body.nivel,
		dias: req.body.dias,
		pagoDiario: req.body.pagoDiario,
		pSubtotal: req.body.pSubtotal,
		pImpuestos: req.body.pImpuestos,
		pTotal: req.body.pTotal,
		
	};
	res.render("registros.html", params);
});

//publicar
router.post("/registros", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		nivel: req.body.nivel,
		dias: req.body.dias,
		pagoDiario: req.body.pagoDiario,
		pSubtotal: req.body.pSubtotal,
		pImpuestos: req.body.pImpuestos,
		pTotal: req.body.pTotal,
	};
	res.render("registros.html", params);
});

//PARTE FINAL: importar o publicar rutas para que app pueda manejar el archivo de rutas
module.exports = router;